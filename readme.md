# Présentation beamer du MRASL

## Contenu

Quelques acétates (*slides*) effectuées avec Beamer pour expliquer ce qu'est le MRASL (*Mobile Robotics and Autonomous Systems Laboratory*), un des laboratoires les plus caféinés et certainement le meilleur de Polytechnique Montréal selon une étude partiale menée rigoureusent au sein de ses membres.

Le but de partager ce repo est également de fournir une petit gabarit (prononcer en français de France *template*) à ses étudiants s'ils souhaitent effectuer un séminaire

### Fichiers

- `pres.tex` le code source en LaTeX, utilisant la librairie Beamer, le thème est Darmstadt mais on peut changer pour PaloAlto que j'aime bien également (voir mes slides ici : http://justincano.com/doc/localisation_robots.pdf );
  - <u>Listes des thèmes  :</u> ici http://deic.uab.es/~iblanes/beamer_gallery/ (ce site est vraiment bien fait pour choisir le thème et la couleur de votre présentation).
- ` pres.pdf` le dernier fichier complilé par `pdflatex` ;
  -  Si on ajoute une slide de biblio, faire comme dans un document LaTeX classique et mettre la biblio dans un environnement `frame` , penser à recompiler avec `bibtex` puis recompiler deux fois avec `pdflatex` pour mettre à jour les numérotations de la bibiographie.
- `fig/` répertoire contenant les figures incorporées dans la présentation.

## Contributeur 

Justin Cano, Doctorant (2019-2023) au MRASL, en cotutelle avec l'ISAE-Supaéro au DÉOS. Contacter ce dernier pour toute suggestion. 
